import React, { Fragment, Component } from 'react';

import Header from '../../components/Header';
import CabecalhoCategoria from './../../components/CabecalhoCategoria';
import ListaProdutos from './../../components/ListaProdutos';
import Newsletter from './../../components/Newsletter';
import Footer from '../../components/Footer';

import data from './../../data/products.json'; // Mockup de produtos

class Categoria extends Component {

    constructor() {
        super();
        this.state = {
            produtos: []
        }
    }

    render() {
        return (
            <Fragment>
                <Header />
                <main className="main">
                    <CabecalhoCategoria />
                    <ListaProdutos produtos={data} />
                </main>
                <Newsletter />
                <Footer />
            </Fragment>
        )
    }


    // componentDidMount(){
    //     fetch('')
    //         .then(response => response.json())            
    // }

}

export default Categoria;