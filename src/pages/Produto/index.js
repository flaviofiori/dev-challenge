import React, { Fragment } from 'react';
import Header from '../../components/Header';
import Newsletter from './../../components/Newsletter';
import Footer from '../../components/Footer';

import InfomacaoProduto from '../../components/InfomacaoProduto';
import ImagensProduto from '../../components/ImagensProduto';
import ProdutoPorClientes from '../../components/ProdutoPorClientes';
import ProdutosSimilares from '../../components/ProdutosSimilares';

import './index.scss';

import data from './../../data/products.json';

function Produto() {
    return (
        <Fragment>
            <Header />

            <main className="main">

                <section className="produto-principal">
                    <ImagensProduto imagens={data[1].items[0].images} />
                    <InfomacaoProduto produto={data[1]}/>
                </section>

                <ProdutosSimilares />

                <ProdutoPorClientes />
            </main>

            <Newsletter />
            <Footer />
        </Fragment>
    )
}

export default Produto;