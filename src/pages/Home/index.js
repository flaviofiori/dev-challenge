import React, { Fragment } from 'react';

import Header from '../../components/Header';
import BannerHome from '../../components/BannerHome';
import CarouselProdutos from '../../components/CarouselProdutos';
import BannersCategoriaHome from '../../components/BannersCategoriaHome';
import Newsletter from './../../components/Newsletter';
import Footer from '../../components/Footer';

import data from './../../data/products.json';

function Home() {

    const produtos = [data[0], data[1], data[2], data[3], data[4]]

    return (
        <Fragment>
            <Header />

            <main className="main">
                <BannerHome />
                <CarouselProdutos titulo="Bestselleters" produtos={produtos} />
                <BannersCategoriaHome />
                <CarouselProdutos titulo="Bestselleters" produtos={produtos} />
                <CarouselProdutos titulo="Instashop" produtos={produtos} instashop="true"/>
            </main>

            <Newsletter />
            <Footer />
        </Fragment>
    )
}

export default Home;