import React, { Fragment, Component } from 'react';
import Prateleira from './../Prateleira';

import './index.scss';

import data from './../../data/products.json';

class ProdutosSimilares extends Component {

    constructor() {
        super();
        this.state = {
            produtos: [data[0], data[1]]
        }
    }

    render() {
        return (
            <Fragment>
                <section className="produtos-similares">
                    <h4 className="produtos-similares__titulo">Looks similares</h4>
                    <ul className="produtos-similares__lista">
                        {
                            this.state.produtos.map((element) => {
                                return (
                                    <Prateleira produto={element} key={element.productId} />
                                )
                            })
                        }
                    </ul>
                </section>
            </Fragment>
        )
    }
}

export default ProdutosSimilares;