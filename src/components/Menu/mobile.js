import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './mobile.scss';
import logo from './../../assets/images/logo.png';

export class NavegacaoMobile extends Component {
    constructor() {
        super();
        this.state = {
            menu: false
        }
    }

    render() {
        return (
            <header className={this.state.menu ? 'header-mobile active' : 'header-mobile'}>
                <div className="grid">
                    <button className="header-mobile__botao header-mobile__botao--menu" onClick={this.mostraMenu}>
                        <i className="icones icones__menu"></i>
                        <span>Menu</span>
                    </button>
                    <figure>
                        <Link to="/">
                            <img src={logo} alt="" />
                        </Link>
                    </figure>
                    <div>
                        <button className="header-mobile__botao header-mobile__botao--busca">
                            <i className="icones icones__busca"></i>
                        </button>
                        <button className="header-mobile__botao header-mobile__botao--sacola">
                            <i className="icones icones__sacola"></i>
                        </button>
                    </div>
                </div>
                <nav className="header-mobile__menu">
                    <button className="header-mobile__botao header-mobile__botao--fechar" onClick={this.fechaMenu}>
                        <i className="icones icones__fechar"></i>
                    </button>
                    <header className="header-mobile__menuCabecalho">
                        <p>
                            <a href="#" className="header-mobile__link">Login</a>
                        </p>
                        <p>
                            <button className="header-mobile__botao header-mobile__botao--whatsapp">
                                <i className="icones icones__whatsapp"></i>
                            </button>
                            <button className="header-mobile__botao header-mobile__botao--busca">
                                <i className="icones icones__estrela"></i>
                            </button>
                        </p>
                    </header>
                    <form className="header-mobile__busca">
                        <div className="header-mobile__grupo">
                            <input type="text" placeholder="Buscar..." className="header-mobile__campo" />
                            <button className="header-mobile__botao header-mobile__botao--form-busca">
                                <i className="icones icones__busca"></i>
                            </button>
                        </div>
                    </form>
                    <ul className="header-mobile__lista">
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Novidades</a>
                        </li>
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Coleção</a>
                        </li>
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Bolsas</a>
                        </li>
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Calçados</a>
                        </li>
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Acessórios</a>
                        </li>
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Intimates</a>
                        </li>
                        <li>
                            <a href="#" className="header-mobile__link header-mobile__link--menu">Sale</a>
                        </li>
                    </ul>
                </nav>
            </header>
        )
    }

    mostraMenu = (event) => {
        event.preventDefault();
        this.setState({ menu: true });
    }

    fechaMenu = (event) => {
        event.preventDefault();
        this.setState({ menu: false });
    }
}
