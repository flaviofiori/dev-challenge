import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './desktop.scss';

import logo from './../../assets/images/logo.png';
import logoInside from './../../assets/images/logo-inside.png';

export class NavegacaoDesktop extends Component {
    constructor() {
        super();
        this.state = {
            menu: false
        }
    }

    render() {
        return (
            <header className="header-desktop">
                <div className="grid">

                    <figure className="header-desktop__logo">
                        <Link to="/">
                            <img src={logo} alt="" />
                        </Link>
                    </figure>

                    <nav className="header-desktop__navegacao">

                        <div className="header-desktop__acoes">
                            <a href="#" className="header-desktop__link">
                                <i className="icones icones__mapa"></i>
                            </a>
                            <a href="#" className="header-desktop__link">
                                <i className="icones icones__telefone"></i>
                            </a>
                        </div>

                        <ul className="header-desktop__menu">
                            <li>
                                <a href="#" className="header-desktop__link header-desktop__link--menu">Novidades</a>
                            </li>
                            <li className={this.state.menu ? 'active' : ''}>
                                <a href="#" className="header-desktop__link header-desktop__link--menu" onClick={this.mostraMenu}>Coleção</a>

                                <div className="header-desktop__segundo-nivel">
                                    <div className="header-desktop__container">
                                        <div className="header-desktop__grupo">
                                            <div className="header-desktop__item header-desktop__item--um">
                                                <p className="header-desktop__titulo"><strong>Categorias</strong></p>
                                                <ul>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Vestido</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Saia</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Top/Blusa</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Short</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Calça</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Blazer</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Camisa</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Colete</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Jaqueta</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Ver tudo</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Macacão</a>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Couro</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Alfaiataria</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Seda</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Tricot</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Club/Noite</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Jeans</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Malha</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Ver tudo</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="header-desktop__grupo">
                                            <div className="header-desktop__item header-desktop__item--dois">
                                                <p className="header-desktop__titulo"><strong>Acessórios</strong></p>
                                                <ul>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Bolsas</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Calçados</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Cinto</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">olar</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Anel</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Brinco</a>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Pulseira/Bracelete</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Echarpe/Lenço</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Ver tudo</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="header-desktop__item header-desktop__item--tres">
                                                <p className="header-desktop__titulo"><strong>Intimates</strong></p>
                                                <ul>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Lingerie</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Underwear</a>
                                                    </li>
                                                    <li>
                                                        <a href="" className="header-desktop__link">Ver tudo</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" className="header-desktop__link header-desktop__link--menu">Joias</a>
                            </li>
                            <li>
                                <a href="#" className="header-desktop__link header-desktop__link--menu">Sale</a>
                            </li>
                            <li>
                                <a href="#" className="header-desktop__link">
                                    <img src={logoInside} alt="" />
                                </a>
                            </li>
                        </ul>

                        <div className="header-desktop__acoes">
                            <a href="#" className="header-desktop__link">
                                <i className="icones icones__login"></i>
                            </a>
                            <a href="#" className="header-desktop__link">
                                <i className="icones icones__sacola-desktop"></i>
                            </a>
                            <a href="#" className="header-desktop__link">
                                <i className="icones icones__busca-desktop"></i>
                            </a>
                        </div>
                    </nav>
                </div>
            </header>
        )
    }

    mostraMenu = (event) => {
        event.preventDefault();
        if (!this.state.menu) {
            this.setState({ menu: true });
        } else {
            this.setState({ menu: false });
        }
    }
}
