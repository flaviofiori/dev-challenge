import React, { Component } from 'react';

import './index.scss';

class InfomacaoProduto extends Component {

    constructor(props) {
        super(props);
        this.state = {
            produto: props.produto
        }
    }

    render() {
        console.log(this.state.produto);
        return (
            <div className="informacao-produto">
                <div className="grid">
                    <p className="informacao-produto__codigo">
                        Ref. {this.state.produto.productReference}
                    </p>
                    <h1 className="informacao-produto__nome">
                        {this.state.produto.productName}
                    </h1>
                    <p className="informacao-produto__preco">
                        <strong className="informacao-produto__preco--por">R$ {this.state.produto.items[0].sellers[0].commertialOffer.Price}</strong>
                        <span className="informacao-produto__preco--parcelado"> ou 10x de R$ {this.state.produto.items[0].sellers[0].commertialOffer.Price / 10} sem juros</span>
                    </p>

                    <div className="informacao-produto__barra-fixa">

                        <select className="informacao-produto__select informacao-produto__select--tamanho">
                            <option value="">Tamanho</option>
                            {
                                this.state.produto.items.map((element) => {
                                    return (
                                        <option value={element.itemId}>{element.Tamanho[0]}</option>
                                    )
                                })
                            }
                        </select>

                        <select className="informacao-produto__select informacao-produto__select--cor">
                            <option value="">Est</option>
                        </select>

                        <button className="informacao-produto__botao informacao-produto__botao--fixo">Comprar</button>
                    </div>

                    <button className="informacao-produto__botao">
                        Comprar
                    </button>
                </div>

                <details className="informacao-produto__detalhes">
                    <summary className="informacao-produto__titulo">Medidas desta peça</summary>
                </details>

                <details className="informacao-produto__detalhes">
                    <summary className="informacao-produto__titulo">Como cuidar deste produto</summary>
                </details>

                <details className="informacao-produto__detalhes">
                    <summary className="informacao-produto__titulo">Duvidas? (5 avaliações)</summary>
                </details>
            </div>
        )
    }

    // goToCheckout(produto) {
    //     alert(`Produto ${produto} adicionado com sucesso!`);
    // }
}

export default InfomacaoProduto;