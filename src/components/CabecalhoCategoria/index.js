import React from 'react';
import './index.scss'

function CabecalhoCategoria() {

    return (
        <section className="cabecalho-categoria">
            <div className="cabecalho-categoria__container">
                <div className="cabecalho-categoria__info">
                    <h1 className="cabecalho-categoria__titulo">Couro</h1>
                    <p className="cabecalho-categoria__text">
                        As peças de couro da ANIMALE são irreverentes e se destacam nas produções. Os shapes assimétricos, detalhes metálicos e as tonalidades mais escuras e vibrantes imprimem sofisticação na hora de compor um look. Renove seu closet para as próximas estações com as roupas de couro femininas da nossa coleção.
                </p>
                    <ul className="cabecalho-categoria__lista">
                        <li>
                            <a href="#" className="cabecalho-categoria__link">Vestido de couro</a>
                        </li>
                        <li>
                            <a href="#" className="cabecalho-categoria__link">Vestido de couro</a>
                        </li>
                        <li>
                            <a href="#" className="cabecalho-categoria__link">Vestido de couro</a>
                        </li>
                        <li>
                            <a href="#" className="cabecalho-categoria__link">Vestido de couro</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    )
}

export default CabecalhoCategoria;