import React, { Fragment } from 'react';

import './index.scss';
import image01 from './../../assets/images/banner-home-categoria-image-01.png'

function BannersCategoriaHome() {

    return (
        <Fragment>
            <section className="banner-home-categoria">
                <div className="grid">
                    <ul className="banner-home-categoria__lista">

                        <li className="banner-home-categoria__item">
                            <figure className="banner-home-categoria__cartao">
                                <img className="banner-home-categoria__imagem" src={image01} alt="" />
                                <figcaption className="banner-home-categoria__nome">
                                    Vestidos
                                </figcaption>
                            </figure>
                        </li>

                        <li className="banner-home-categoria__item">
                            <figure className="banner-home-categoria__cartao">
                                <img className="banner-home-categoria__imagem" src={image01} alt="" />
                                <figcaption className="banner-home-categoria__nome">
                                    Seda
                                </figcaption>
                            </figure>
                        </li>

                    </ul>
                </div>
            </section>
        </Fragment>
    )
}

export default BannersCategoriaHome;