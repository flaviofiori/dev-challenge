import React from 'react';
import Slider from 'react-slick';

import imagem1 from './../../assets/images/bannerHome-imagem-01.png';
import imagem2 from './../../assets/images/bannerHome-imagem-02.png';

import './index.scss';

function BannerHome() {

    let arrayImages = [];
    arrayImages.push(imagem1, imagem2);

    const settings = {
        arrows: true,
        dots: true,
        slidesToShow: 1,
        unslick: true,
        responsive: [
            {
                breakpoint: 999,
                settings: {
                    unslick: false
                }
            },
        ]
    }

    return (
        <section className="banner-home">
            <Slider {...settings}>
                {
                    arrayImages.map((element, index) => {
                        return (
                            <figure className="banner-home__figure" key={index}>
                                <img src={element} alt="" className="banner-home__imagem"/>
                                <figcaption className="banner-home__info">
                                    <p className="banner-home__text">
                                        <span className="banner-home__small">Lançamento</span>
                                        <span className="banner-home__title">LOREM IPSUM</span>
                                        <a href="#" className="banner-home__link">Shop now</a>
                                    </p>
                                </figcaption>
                            </figure>
                        )
                    })
                }
            </Slider>
        </section>
    )
}

export default BannerHome;