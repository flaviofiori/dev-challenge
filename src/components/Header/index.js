import React, { Fragment } from 'react';
import { NavegacaoMobile } from './../Menu/mobile';
import { NavegacaoDesktop } from './../Menu/desktop';

function Header() {
    return (
        <Fragment>
            <NavegacaoMobile />
            <NavegacaoDesktop />
        </Fragment>
    )
}


export default Header;