import React, { Component } from 'react';
import Prateleira from './../Prateleira';

import './index.scss';

class ListaProdutos extends Component {

    constructor(props) {
        super(props);

        this.state = {
            produtos: props.produtos
        }
    }

    render() {
        return (

            <section className="lista-produtos">
                <div class="grid">
                    <ul className="lista-produtos__container">
                        {
                            this.state.produtos.map((element) => {
                                return (
                                    <Prateleira produto={element} key={element.productId} />
                                )
                            })
                        }
                    </ul>
                </div>
            </section>

        )
    }
}


export default ListaProdutos;