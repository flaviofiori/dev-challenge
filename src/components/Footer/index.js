import React from 'react';
import './index.scss';

function Footer() {
    return (
        <footer className="footer">
            <NavegacaoMobile />
            <CopyRight />
        </footer>
    )
}

function NavegacaoMobile() {
    return (
        <nav className="navegacaoMobile">
            <ul>
                <li>
                    <details className="navegacaoMobile__accordion">
                        <summary className="navegacaoMobile__titulo">A marca</summary>
                        <p className="navegacaoMobile__texto">Conteúdo de marca</p>
                    </details>
                </li>
                <li>
                    <details className="navegacaoMobile__accordion">
                        <summary className="navegacaoMobile__titulo">Minha Conta</summary>
                        <p className="navegacaoMobile__texto">Conteúdo de minha conta</p>
                    </details>
                </li>
                <li>
                    <details className="navegacaoMobile__accordion">
                        <summary className="navegacaoMobile__titulo">Políticas</summary>
                        <p className="navegacaoMobile__texto">Conteúdo de políticas</p>
                    </details>
                </li>
                <li>
                    <details className="navegacaoMobile__accordion">
                        <summary className="navegacaoMobile__titulo">Forma de pagamento</summary>
                        <p className="navegacaoMobile__texto">Conteúdo de forma de pagamento</p>
                    </details>
                </li>
                <li>
                    <details className="navegacaoMobile__accordion">
                        <summary className="navegacaoMobile__titulo">Redes sociais</summary>
                        <p className="navegacaoMobile__texto">Conteúdo de redes sociais</p>
                    </details>
                </li>
                <li>
                    <details className="navegacaoMobile__accordion">
                        <summary className="navegacaoMobile__titulo">Outras coleções</summary>
                        <p className="navegacaoMobile__texto">Conteúdo de outras coleções</p>
                    </details>
                </li>
            </ul>
        </nav>
    )
}

function CopyRight() {
    return (

        <div className="footer__copyRight">
            <div className="grid">
                <p className="footer__texto">
                    RBX RIO COMÉRCIO DE ROUPAS LTDA. ESTRADA DOS BANDEIRANTES, 1.700 - GALPÃO 03, ARMAZÉM 104 - TAQUARA, RIO DE JANEIRO, RJ - CEP: 22775-109. CNPJ: 10.285.590/0002-80.
                </p>
            </div>
        </div>

    )
}

export default Footer;