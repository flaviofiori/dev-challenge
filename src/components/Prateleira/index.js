import React, { Fragment } from 'react';

function Prateleira(props) {

    const { productName, items } = props.produto;

    return (
        <Fragment>
            <li className="lista-produtos__item">
                <figure className="lista-produtos__figure">
                    <img src={items[0].images[0].imageUrl} alt={productName} className="lista-produtos__imagem lista-produtos__imagem--frente" />
                    <img src={items[0].images[1].imageUrl} alt={productName} className="lista-produtos__imagem lista-produtos__imagem--verso" />
                </figure>
                <div>
                    <h4 className="lista-produtos__nome">{productName}</h4>
                    <p>
                        <span className="lista-produtos__preco lista-produtos__preco--de">
                            R$ {items[0].sellers[0].commertialOffer.Price}
                        </span>
                        <span className="lista-produtos__preco lista-produtos__preco--parcelado">
                            10x de R$ 119,80
                        </span>
                    </p>
                    <p>
                        <button className="lista-produtos__botao">Quick Shop</button>
                    </p>
                </div>
            </li>
        </Fragment>
    )
}

export default Prateleira;