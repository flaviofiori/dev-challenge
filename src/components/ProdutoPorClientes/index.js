import React from 'react';
import './index.scss';

function ProdutoPorClientes() {
    return (
        <section className="produto-por-clientes">
            <div className="grid">
                <div className="produto-por-clientes__secao">
                    <h3 className="produto-por-clientes__titulo">
                        <span>Avaliações de produto</span>
                    </h3>
                    <p className="produto-por-clientes__texto">
                        Tem esse produto?<br />
                        Seja o primeiro a avaliação-lo!
                    </p>
                    <p className="produto-por-clientes__texto">
                        <a href="#" className="produto-por-clientes__botao">Enviar avaliação</a>
                    </p>
                </div>
                <div className="produto-por-clientes__secao">
                    <h3 className="produto-por-clientes__titulo">
                        <span>Dúvidas das clientes</span>
                    </h3>
                    <p className="produto-por-clientes__texto">
                        Você tem alguma dúvida sobre este produto?
                    </p>
                    <form className="produto-por-clientes__form">
                        <label>
                            <textarea className="produto-por-clientes__campo" placeholder="Digite sua dúvida (máximo 200 caracteres)"></textarea>
                        </label>
                        <button className="produto-por-clientes__botao">Enviar pergunta</button>
                    </form>
                </div>
            </div>
        </section>
    )
}

export default ProdutoPorClientes;