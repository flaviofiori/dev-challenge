import React, { Fragment, Component } from 'react';
import Slider from 'react-slick';
import Prateleira from './../Prateleira';

import './index.scss';

class CarouselProdutos extends Component {

    constructor(props) {
        super(props);

        this.state = {
            titulo: props.titulo,
            produtos: props.produtos
        }
    }

    render() {

        const settings = {
            arrows: true,
            dots: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 999,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
            ]
        }

        return (
            <Fragment>
                <section className="carousel-produtos">
                    <div className="grid">
                        <h2 className="carousel-produtos__titulo">
                            <span>{this.state.titulo}</span>
                        </h2>

                        <Slider {...settings}>
                            {
                                this.state.produtos.map((element) => {
                                    return (
                                        <Prateleira produto={element} key={element.productId} />
                                    )
                                })
                            }
                        </Slider>

                    </div>
                </section>
            </Fragment>
        )
    }
}


export default CarouselProdutos;