import React, { Component } from 'react';
import Slider from 'react-slick';

import './index.scss';

class ImagensProduto extends Component {

    constructor(props) {
        super(props);

        const { imagens } = props;

        this.state = {
            imagens: imagens
        }
    }

    render() {

        const settings = {
            arrows: true,
            dots: false,
            slidesToShow: 1,
            unslick: true,
            responsive: [
                {
                    breakpoint: 999,
                    settings: {
                        unslick: false
                    }
                },
            ]
        };

        return (
            <div className="produto-imagens">
                <Slider {...settings}>
                    {
                        this.state.imagens.map((element, index) => {
                            if (index < 4) {
                                return (
                                    <figure key={element.imageId}>
                                        <img src={element.imageUrl} alt="" />
                                    </figure>
                                )
                            }
                        })
                    }
                </Slider>
            </div>
        )
    }
}

export default ImagensProduto;