import React from 'react';
import './index.scss';

function Newsletter() {

    return (
        <section className="newsletter">
            <div className="grid">
                <div className="newsletter__container">
                    <div className="newsletter__info">
                        <h4 className="newsletter__titulo">Newsletter</h4>
                        <p className="newsletter__texto">
                            Cadastre-se para receber nossas<br />
                        novidades e promoções:
                    </p>
                    </div>
                    <form className="newsletter__form">
                        <div className="newsletter__grupo">
                            <input type="text" placeholder="Nome" className="newsletter__campo" />
                        </div>
                        <div className="newsletter__grupo">
                            <input type="text" placeholder="E-mail" className="newsletter__campo" />
                            <button className="newsletter__botao">Ok</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    )
}

export default Newsletter;