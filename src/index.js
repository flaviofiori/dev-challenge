import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import './reset.scss';

import Home from './pages/Home';
import Categoria from './pages/Categoria';
import Produto from './pages/Produto';

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/categoria" exact component={Categoria} />
                <Route path="/produto" exact component={Produto} />
            </Switch>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);
