# Teste Grupo Soma

O teste foi desenvolvido com a ferramenta [create-react-app](https://create-react-app.dev/docs/getting-started/)

## Instalação

Instale as dependências com:
```
npm install
```

## Comandos 

Rode o comando abaixo para iniciar o projeto:
```
npm start
```

## Páginas
* [Home](http://localhost:3000/)
* [Categoria](http://localhost:3000/categoria)
* [Produto](http://localhost:3000/produto)

## Considerações

Alguns blocos do layout .xd, principalmente na versão desktop estavam como imagens. Por isso só adaptei o conteúdo mobile para as telas maiores. 

Conforme conversado com a recrutadora, o teste não foi finalizado por completo.

Pendências: filtros na página de categoria, adicionar produto ao carrinho e ver mais produtos